﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VVE.Hortus.Domain.Users;

namespace VVE.Hortus.Domain.Pictures
{
    public class Picture
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(256)]
        public string Name { get; set; }
        public string FullPath { get; set; }
        [MaxLength(50)]
        public string Caption { get; set; }
        public StoreUser User { get; set; }
    }
}
