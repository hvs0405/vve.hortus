﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VVE.Hortus.Domain.Documents
{
    public class Document
    {
        public int Id { get; set; }
        [Required]
        [MinLength(5, ErrorMessage = "Too Short")]
        public string FileName { get; set; }
        [Required]
        [RegularExpression(@"^(?:[a-zA-Z]\:|\\\\[\w\.]+\\[\w.$]+)\\(?:[\w]+\\)*\w([\w.])+$", ErrorMessage = "Not a valid File Location")]
        public string FileLocation { get; set; }
        public DateTime CreateDate { get; set; }
        public string Year { get; set; }
        public DateTime UploadDate { get; set; } = DateTime.Now;
    }
}
