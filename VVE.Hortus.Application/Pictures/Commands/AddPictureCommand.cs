﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using VVE.Hortus.Database;

namespace VVE.Hortus.Application.Pictures.Commands
{

    public class AddPictureCommand: IAddPictureCommand
    {
        private readonly IVVEHortusRepository _repo;

        public AddPictureCommand(IVVEHortusRepository repo)
        {
            _repo = repo;
        }

        public void Execute(IFormFile file)
        {

                if (file != null)
                {
                    var model = new AddPictureModel() {
                        Name = file.FileName,
                    };

                    _repo.AddEntity(model);
                    _repo.SaveAll();
                }
            

        }
        
    }
}
