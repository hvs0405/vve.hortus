﻿using Microsoft.AspNetCore.Http;

namespace VVE.Hortus.Application.Pictures.Commands
{
    public interface IAddPictureCommand
    {
        void Execute(IFormFile file);
    }
}
