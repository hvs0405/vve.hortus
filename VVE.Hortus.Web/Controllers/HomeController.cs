﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using VVE.Hortus.Application.Pictures.Commands;
using VVE.Hortus.Database;
using VVE.Hortus.IO;
using VVE.Hortus.Web.Models;

namespace VVE.Hortus.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IVVEHortusRepository _repo;
        private readonly ICopyFiles _copyFiles;
        private readonly ILogger<HomeController> _log;
        private readonly IAddPictureCommand _addPictures;

        public HomeController(IVVEHortusRepository repo, 
            ICopyFiles copyFiles,
            ILogger<HomeController> log,
            IAddPictureCommand addPictures)
        {
            _repo = repo;
            _copyFiles = copyFiles;
            _log = log;
            _addPictures = addPictures;
        }

        public IActionResult Index()
        {
            return View();
        }


        [HttpGet("files")]
        public IActionResult Documents()
        {
            return View();
        }

        [HttpPost("files")]
        public IActionResult Documents(DocumentViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Save File
            }

            return View();
        }

        //[Authorize]
        [HttpGet("pictures")]
        public IActionResult Pictures()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewBag.Title = "Over ons";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost("pictures")]
        public async Task<IActionResult> Pictures(IFormFile files)
        {
            try
            {
                _addPictures.Execute(files);
            }
            catch (Exception ex)
            {
                _log.LogError($"Failde to save picture(s): {ex}");
                var r = _repo.GetAllPictures();
                return View(r);
            }

            try
            {
                await _copyFiles.Execute(files, "img");

            }
            catch (Exception ex)
            {
                _log.LogError($"Failde to copy picture(s): {ex}");
            }

            var results = _repo.GetAllPictures();
            return View(results);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
