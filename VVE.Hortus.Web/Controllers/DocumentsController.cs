﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using VVE.Hortus.Database;
using VVE.Hortus.Domain.Documents;

namespace VVE.Hortus.Web.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    [Produces("application/json")]
    //[Authorize(AuthenticationSchemes =JwtBearerDefaults.AuthenticationScheme)]
    public class DocumentsController : Controller
    {
        private readonly IVVEHortusRepository _repo;
        private readonly ILogger<DocumentsController> _log;

        public DocumentsController(IVVEHortusRepository repo, ILogger<DocumentsController> log)
        {
            _repo = repo;
            _log = log;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_repo.GetAllDocuments());
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get documents: {ex}");
                return BadRequest("Failed to get documents");
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            try
            {
                var picture = _repo.GetDocumentById(id);

                if (picture != null) return Ok(picture);
                else return NotFound();
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get documents: {ex}");
                return BadRequest("Failed to get documents");
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody]Document model )
        {
            try
            {
                _repo.AddEntity(model);
                if (_repo.SaveAll())
                {
                    return Created($"/api/pictures/{model.Id}", model);
                }
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to save documents: {ex}");
            }

            return BadRequest("Failed to save documents");
        }


    }

}
