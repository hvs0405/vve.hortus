﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VVE.Hortus.Database;
using VVE.Hortus.Domain.Pictures;
using VVE.Hortus.IO;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http.Headers;

namespace VVE.Hortus.Web.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    [Produces("application/json")]
    //[Authorize(AuthenticationSchemes =JwtBearerDefaults.AuthenticationScheme)]
    public class PicturesController : Controller
    {
        private readonly IVVEHortusRepository _repo;
        private readonly ILogger<PicturesController> _log;
        private readonly IHostingEnvironment _environment;

        public PicturesController(IVVEHortusRepository repo, ILogger<PicturesController> log, IHostingEnvironment environment)
        {
            _repo = repo;
            _log = log;
            _environment = environment;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_repo.GetAllPictures());
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get pictures: {ex}");
                return BadRequest("Failed to get pictures");
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            try
            {
                var picture = _repo.GetPictureById(id);

                if (picture != null) return Ok(picture);
                else return NotFound();
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get pictures: {ex}");
                return BadRequest("Failed to get pictures");
            }
        }

        [HttpPost]
        public IActionResult Post()
        {
            try
            {
                var files = HttpContext.Request.Form.Files;
                var path = Path.Combine(_environment.WebRootPath, "img\\");
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        var fileName = ContentDispositionHeaderValue.Parse
                            (file.ContentDisposition).FileName.Trim('"');
                        var fullName = path + fileName;

                        using (FileStream fs = System.IO.File.Create(fullName))
                        {
                            file.CopyTo(fs);
                            fs.Flush();
                        }

                        var  picture = new Picture();
                        picture.Name = fileName;
                        picture.FullPath = "img/" + fileName;

                        _repo.AddEntity(picture);
                        if (_repo.SaveAll())
                        {
                            return Created($"/api/pictures/: {fileName} ", fileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to save pictures: {ex}");
            }

            return BadRequest("Failed to save picture");
        }
    }
}
