import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var DocumentList = /** @class */ (function () {
    function DocumentList() {
        this.documents = [
            {
                FileName: "Filename 1"
            },
            {
                FileName: "Filename 2"
            },
            {
                FileName: "Filename 3"
            },
        ];
    }
    DocumentList = tslib_1.__decorate([
        Component({
            selector: "document-list",
            templateUrl: "documentList.component.html",
            styleUrls: []
        })
    ], DocumentList);
    return DocumentList;
}());
export { DocumentList };
//# sourceMappingURL=documentList.component.js.map