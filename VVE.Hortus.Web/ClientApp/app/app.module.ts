import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { PictureList } from './vvehortus/pictureList.component'
import { DocumentList } from './vvehortus/documentList.component'
import { VVEHortus } from './vvehortus/vvehortus.component'
import { Pictures } from './vvehortus/pictures.component'
import { DataService } from './shared/dataService';
import { CarouselModule } from 'ngx-bootstrap';

import { RouterModule } from "@angular/router";

let routes = [
    { path: "", component: Pictures },
    { path: "picturelist", component: PictureList },
];

@NgModule({
    declarations: [
        AppComponent,
        PictureList,
        DocumentList,
        VVEHortus,
        Pictures
    ],
    imports: [
        CarouselModule,
        BrowserModule,
        HttpClientModule,
        RouterModule.forRoot(routes, {
            useHash: true,
            enableTracing: false,
        }),
        CarouselModule.forRoot()
    ],
    providers: [DataService],
    bootstrap: [AppComponent]
})
export class AppModule { }
