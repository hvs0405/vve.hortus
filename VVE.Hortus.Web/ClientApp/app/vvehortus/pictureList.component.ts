﻿import { Component, OnInit } from '@angular/core';
import { DataService } from "../shared/dataService";
import { Picture } from "../shared/picture"
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'picture-list',
    templateUrl: "pictureList.component.html",
    styleUrls: []
})

export class PictureList implements OnInit {
    constructor(private data: DataService, private http: HttpClient) {
    }
    url = "img/"
    public pictures: Picture[] = [];
    public picture: Picture = new Picture();

    ngOnInit(): void {
        this.data.loadPictures()
            .subscribe(success => {
                if (success) {
                    this.pictures = this.data.pictures;
                }
            });
    }

    selectedFile: File = null;

    onFileSelected(event) {
        this.selectedFile = <File>event.target.files[0];

        console.log(event);

    }

    addPicture() {
        const fd = new FormData();
        fd.append('image', this.selectedFile, this.selectedFile.name);

            this.data.addPictures(fd)
                .subscribe(
                (data) => {
                    console.log(data);

                    this.ngOnInit();

                }),
                err => {
                    console.log("Error");
                }  
    }
}

