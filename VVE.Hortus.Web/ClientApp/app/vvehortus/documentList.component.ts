﻿import { Component, OnInit } from '@angular/core';
import { DataService } from "../shared/dataService";
import { Document } from "../shared/document"

@Component({
    selector: "document-list",
    templateUrl: "documentList.component.html",
    styleUrls: []
})

export class DocumentList implements OnInit {
    constructor(private data: DataService) {
    }

    public documents: Document[] = [];

    ngOnInit(): void {
        this.data.loadDocuments()
            .subscribe(success => {
                if (success) {
                    this.documents = this.data.documents;
                }
            });
    }
}