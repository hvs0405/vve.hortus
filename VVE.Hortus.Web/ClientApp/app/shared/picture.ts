﻿export class Picture {
    id: number;
    name: string;
    fullPath: string;
    caption: string;
    user?: any;
}