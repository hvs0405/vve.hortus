﻿export interface Document {
    id: number;
    fileName: string;
    fileLocation: string;
    createDate: Date;
    year: string;
    uploadDate: Date;
}