﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs"
import { map } from 'rxjs/operators';
import { Document } from "./document";
import { Picture } from "./picture";

@Injectable()
export class DataService {

    constructor(private http: HttpClient) { }

    public documents: Document[] = [];
    public pictures: Picture[] = [];
    public picture: Picture = new Picture;

    loadDocuments(): Observable<boolean> {
        return this.http.get("/api/Documents")
            .pipe(
                map((data: any[]) => {
                    this.documents = data;
                    return true;
                }));
    }

    loadPictures(): Observable<boolean> {
        return this.http.get("/api/Pictures")
            .pipe(
                map((data: any[]) => {
                    this.pictures = data;
                    return true;
                }));
    }

    addPictures(formData: FormData) {
        console.log("addPictures ")

        return this.http.post("/api/Pictures", formData);

    }
}