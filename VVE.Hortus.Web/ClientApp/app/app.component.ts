import { Component } from '@angular/core';

@Component({
  selector: 'the-pictures',
  templateUrl: "./app.component.html",
  styles: []
})
export class AppComponent {
  title = 'VVE Hortus';
}
