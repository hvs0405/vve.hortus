(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "../$$_lazy_route_resource lazy recursive":
/*!*******************************************************!*\
  !*** ../$$_lazy_route_resource lazy namespace object ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../node_modules/raw-loader/index.js!./app/app.component.html":
/*!***********************************************************!*\
  !*** ../node_modules/raw-loader!./app/app.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "../node_modules/raw-loader/index.js!./app/vvehortus/documentList.component.html":
/*!******************************************************************************!*\
  !*** ../node_modules/raw-loader!./app/vvehortus/documentList.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"document-info col-md-4\" *ngFor=\"let p of documents\">\r\n        <ul>\r\n            <li>{{ p.fileName }}</li>\r\n            <li>{{ p.fileLocation }}</li>\r\n            <li>{{ p.createDate | date:\"dd-MM-yyyy hh:mm:ss\" }}</li>\r\n            <li>{{ p.year }}</li>\r\n            <li>{{ p.uploadDate | date:\"dd-MM-yyyy hh:mm:ss\" }}</li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n<a routerlink=\"/add-document\" class=\"btn btn-success\">Add Document(s)</a>\r\n"

/***/ }),

/***/ "../node_modules/raw-loader/index.js!./app/vvehortus/pictureList.component.html":
/*!*****************************************************************************!*\
  !*** ../node_modules/raw-loader!./app/vvehortus/pictureList.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n        <carousel class=\"text-center\">\r\n            <div *ngFor=\"let p of pictures\">\r\n                <slide>\r\n                    <img class=\"img-main-carousel\" src=\"{{p.fullPath}}\" alt=\"{{p.name}}\" style=\"display: block; width: 100%;\">\r\n                    <div class=\"carousel-caption d-none d-md-block\">\r\n                        <h1>{{p.name}}</h1>\r\n                    </div>\r\n                </slide>\r\n            </div>\r\n        </carousel>\r\n    </div>\r\n</div>\r\n\r\n<input type=\"file\" (change)=\"onFileSelected($event)\" />\r\n<button type=\"button\" (click)=\"addPicture()\">Upload</button>\r\n<!--<a routerlink=\"/add-picture\" class=\"btn btn-success\">Add Picture(s)</a>-->\r\n"

/***/ }),

/***/ "../node_modules/raw-loader/index.js!./app/vvehortus/pictures.component.html":
/*!**************************************************************************!*\
  !*** ../node_modules/raw-loader!./app/vvehortus/pictures.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\" >\r\n    <div class=\"col-md-12\">\r\n        <h3>{{title}}</h3>\r\n        <picture-list></picture-list>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../node_modules/raw-loader/index.js!./app/vvehortus/vvehortus.component.html":
/*!***************************************************************************!*\
  !*** ../node_modules/raw-loader!./app/vvehortus/vvehortus.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n        <h3> {{title}} </h3>\r\n        <picture-list></picture-list>\r\n        <!--<document-list></document-list>-->\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./app/app.component.ts":
/*!******************************!*\
  !*** ./app/app.component.ts ***!
  \******************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'VVE Hortus';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'the-pictures',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "../node_modules/raw-loader/index.js!./app/app.component.html")
    })
], AppComponent);



/***/ }),

/***/ "./app/app.module.ts":
/*!***************************!*\
  !*** ./app/app.module.ts ***!
  \***************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "../node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./app/app.component.ts");
/* harmony import */ var _vvehortus_pictureList_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./vvehortus/pictureList.component */ "./app/vvehortus/pictureList.component.ts");
/* harmony import */ var _vvehortus_documentList_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./vvehortus/documentList.component */ "./app/vvehortus/documentList.component.ts");
/* harmony import */ var _vvehortus_vvehortus_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./vvehortus/vvehortus.component */ "./app/vvehortus/vvehortus.component.ts");
/* harmony import */ var _vvehortus_pictures_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./vvehortus/pictures.component */ "./app/vvehortus/pictures.component.ts");
/* harmony import */ var _shared_dataService__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./shared/dataService */ "./app/shared/dataService.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap */ "../node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm2015/router.js");












let routes = [
    { path: "", component: _vvehortus_pictures_component__WEBPACK_IMPORTED_MODULE_8__["Pictures"] },
    { path: "picturelist", component: _vvehortus_pictureList_component__WEBPACK_IMPORTED_MODULE_5__["PictureList"] },
];
let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _vvehortus_pictureList_component__WEBPACK_IMPORTED_MODULE_5__["PictureList"],
            _vvehortus_documentList_component__WEBPACK_IMPORTED_MODULE_6__["DocumentList"],
            _vvehortus_vvehortus_component__WEBPACK_IMPORTED_MODULE_7__["VVEHortus"],
            _vvehortus_pictures_component__WEBPACK_IMPORTED_MODULE_8__["Pictures"]
        ],
        imports: [
            ngx_bootstrap__WEBPACK_IMPORTED_MODULE_10__["CarouselModule"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_11__["RouterModule"].forRoot(routes, {
                useHash: true,
                enableTracing: false,
            }),
            ngx_bootstrap__WEBPACK_IMPORTED_MODULE_10__["CarouselModule"].forRoot()
        ],
        providers: [_shared_dataService__WEBPACK_IMPORTED_MODULE_9__["DataService"]],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./app/shared/dataService.ts":
/*!***********************************!*\
  !*** ./app/shared/dataService.ts ***!
  \***********************************/
/*! exports provided: DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "../node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _picture__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./picture */ "./app/shared/picture.ts");





let DataService = class DataService {
    constructor(http) {
        this.http = http;
        this.documents = [];
        this.pictures = [];
        this.picture = new _picture__WEBPACK_IMPORTED_MODULE_4__["Picture"];
    }
    loadDocuments() {
        return this.http.get("/api/Documents")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((data) => {
            this.documents = data;
            return true;
        }));
    }
    loadPictures() {
        return this.http.get("/api/Pictures")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((data) => {
            this.pictures = data;
            return true;
        }));
    }
    addPictures(formData) {
        console.log("addPictures ");
        return this.http.post("/api/Pictures", formData);
    }
};
DataService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
DataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()
], DataService);



/***/ }),

/***/ "./app/shared/picture.ts":
/*!*******************************!*\
  !*** ./app/shared/picture.ts ***!
  \*******************************/
/*! exports provided: Picture */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Picture", function() { return Picture; });
class Picture {
}


/***/ }),

/***/ "./app/vvehortus/documentList.component.ts":
/*!*************************************************!*\
  !*** ./app/vvehortus/documentList.component.ts ***!
  \*************************************************/
/*! exports provided: DocumentList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DocumentList", function() { return DocumentList; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _shared_dataService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/dataService */ "./app/shared/dataService.ts");



let DocumentList = class DocumentList {
    constructor(data) {
        this.data = data;
        this.documents = [];
    }
    ngOnInit() {
        this.data.loadDocuments()
            .subscribe(success => {
            if (success) {
                this.documents = this.data.documents;
            }
        });
    }
};
DocumentList.ctorParameters = () => [
    { type: _shared_dataService__WEBPACK_IMPORTED_MODULE_2__["DataService"] }
];
DocumentList = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "document-list",
        template: __webpack_require__(/*! raw-loader!./documentList.component.html */ "../node_modules/raw-loader/index.js!./app/vvehortus/documentList.component.html")
    })
], DocumentList);



/***/ }),

/***/ "./app/vvehortus/pictureList.component.ts":
/*!************************************************!*\
  !*** ./app/vvehortus/pictureList.component.ts ***!
  \************************************************/
/*! exports provided: PictureList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PictureList", function() { return PictureList; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _shared_dataService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/dataService */ "./app/shared/dataService.ts");
/* harmony import */ var _shared_picture__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/picture */ "./app/shared/picture.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm2015/http.js");





let PictureList = class PictureList {
    constructor(data, http) {
        this.data = data;
        this.http = http;
        this.url = "img/";
        this.pictures = [];
        this.picture = new _shared_picture__WEBPACK_IMPORTED_MODULE_3__["Picture"]();
        this.selectedFile = null;
    }
    ngOnInit() {
        this.data.loadPictures()
            .subscribe(success => {
            if (success) {
                this.pictures = this.data.pictures;
            }
        });
    }
    onFileSelected(event) {
        this.selectedFile = event.target.files[0];
        console.log(event);
    }
    addPicture() {
        const fd = new FormData();
        fd.append('image', this.selectedFile, this.selectedFile.name);
        this.data.addPictures(fd)
            .subscribe((data) => {
            console.log(data);
            this.ngOnInit();
        }),
            err => {
                console.log("Error");
            };
    }
};
PictureList.ctorParameters = () => [
    { type: _shared_dataService__WEBPACK_IMPORTED_MODULE_2__["DataService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] }
];
PictureList = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'picture-list',
        template: __webpack_require__(/*! raw-loader!./pictureList.component.html */ "../node_modules/raw-loader/index.js!./app/vvehortus/pictureList.component.html")
    })
], PictureList);



/***/ }),

/***/ "./app/vvehortus/pictures.component.ts":
/*!*********************************************!*\
  !*** ./app/vvehortus/pictures.component.ts ***!
  \*********************************************/
/*! exports provided: Pictures */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Pictures", function() { return Pictures; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm2015/core.js");


let Pictures = class Pictures {
};
Pictures = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'pictures',
        template: __webpack_require__(/*! raw-loader!./pictures.component.html */ "../node_modules/raw-loader/index.js!./app/vvehortus/pictures.component.html")
    })
], Pictures);



/***/ }),

/***/ "./app/vvehortus/vvehortus.component.ts":
/*!**********************************************!*\
  !*** ./app/vvehortus/vvehortus.component.ts ***!
  \**********************************************/
/*! exports provided: VVEHortus */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VVEHortus", function() { return VVEHortus; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm2015/core.js");


let VVEHortus = class VVEHortus {
};
VVEHortus = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "vve-hortus",
        template: __webpack_require__(/*! raw-loader!./vvehortus.component.html */ "../node_modules/raw-loader/index.js!./app/vvehortus/vvehortus.component.html")
    })
], VVEHortus);



/***/ }),

/***/ "./environments/environment.ts":
/*!*************************************!*\
  !*** ./environments/environment.ts ***!
  \*************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./main.ts":
/*!*****************!*\
  !*** ./main.ts ***!
  \*****************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "../node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***********************!*\
  !*** multi ./main.ts ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\git\vve.hortus\VVE.Hortus.Web\ClientApp\main.ts */"./main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map