import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let AppComponent = class AppComponent {
    constructor() {
        this.title = 'VVE Hortus';
    }
};
AppComponent = tslib_1.__decorate([
    Component({
        selector: 'the-pictures',
        templateUrl: "./app.component.html",
        styles: []
    })
], AppComponent);
export { AppComponent };
//# sourceMappingURL=app.component.js.map