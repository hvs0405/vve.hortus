import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import { map } from 'rxjs/operators';
import { Picture } from "./picture";
let DataService = class DataService {
    constructor(http) {
        this.http = http;
        this.documents = [];
        this.pictures = [];
        this.picture = new Picture;
    }
    loadDocuments() {
        return this.http.get("/api/Documents")
            .pipe(map((data) => {
            this.documents = data;
            return true;
        }));
    }
    loadPictures() {
        return this.http.get("/api/Pictures")
            .pipe(map((data) => {
            this.pictures = data;
            return true;
        }));
    }
    addPictures(formData) {
        console.log("addPictures ");
        return this.http.post("/api/Pictures", formData);
    }
};
DataService = tslib_1.__decorate([
    Injectable()
], DataService);
export { DataService };
//# sourceMappingURL=dataService.js.map