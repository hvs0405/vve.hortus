import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Picture } from "../shared/picture";
let PictureList = class PictureList {
    constructor(data, http) {
        this.data = data;
        this.http = http;
        this.url = "img/";
        this.pictures = [];
        this.picture = new Picture();
        this.selectedFile = null;
    }
    ngOnInit() {
        this.data.loadPictures()
            .subscribe(success => {
            if (success) {
                this.pictures = this.data.pictures;
            }
        });
    }
    onFileSelected(event) {
        this.selectedFile = event.target.files[0];
        console.log(event);
    }
    addPicture() {
        const fd = new FormData();
        fd.append('image', this.selectedFile, this.selectedFile.name);
        ////console.log("addPicture to filesystem");
        //this.http.post("img", fd, {
        //    reportProgress: true,
        //    observe: 'events'
        //    })
        //    .subscribe(events => {
        //        console.log(events)
        //    });
        //console.log("addPicture " + this.selectedFile.name);
        //var picture: Picture = new Picture();
        //picture.name = this.selectedFile.name;
        //picture.fullPath = "img/" + this.selectedFile.name;
        //console.log("addPicture to db");
        this.data.addPictures(fd).subscribe();
    }
};
PictureList = tslib_1.__decorate([
    Component({
        selector: 'picture-list',
        templateUrl: "pictureList.component.html",
        styleUrls: []
    })
], PictureList);
export { PictureList };
//# sourceMappingURL=pictureList.component.js.map