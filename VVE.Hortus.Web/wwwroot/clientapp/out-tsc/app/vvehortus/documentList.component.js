import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let DocumentList = class DocumentList {
    constructor(data) {
        this.data = data;
        this.documents = [];
    }
    ngOnInit() {
        this.data.loadDocuments()
            .subscribe(success => {
            if (success) {
                this.documents = this.data.documents;
            }
        });
    }
};
DocumentList = tslib_1.__decorate([
    Component({
        selector: "document-list",
        templateUrl: "documentList.component.html",
        styleUrls: []
    })
], DocumentList);
export { DocumentList };
//# sourceMappingURL=documentList.component.js.map