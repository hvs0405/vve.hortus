import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { DataService } from "../shared/dataService";
var PictureList = /** @class */ (function () {
    function PictureList(data) {
        this.data = data;
        this.documents = [];
    }
    PictureList.prototype.ngOnInit = function () {
        var _this = this;
        this.data.loadDocuments()
            .subscribe(function (success) {
            if (success) {
                _this.documents = _this.data.documents;
            }
        });
    };
    PictureList = tslib_1.__decorate([
        Component({
            selector: "picture-list",
            templateUrl: "pictureList.component.html",
            styleUrls: []
        }),
        tslib_1.__metadata("design:paramtypes", [DataService])
    ], PictureList);
    return PictureList;
}());
export { PictureList };
//# sourceMappingURL=pictureList.component.js.map