import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { DataService } from "../shared/dataService";
var DocumentList = /** @class */ (function () {
    function DocumentList(data) {
        this.data = data;
        this.documents = [];
    }
    DocumentList.prototype.ngOnInit = function () {
        var _this = this;
        this.data.loadDocuments()
            .subscribe(function (success) {
            if (success) {
                _this.documents = _this.data.documents;
            }
        });
    };
    DocumentList = tslib_1.__decorate([
        Component({
            selector: "document-list",
            templateUrl: "documentList.component.html",
            styleUrls: []
        }),
        tslib_1.__metadata("design:paramtypes", [DataService])
    ], DocumentList);
    return DocumentList;
}());
export { DocumentList };
//# sourceMappingURL=documentList.component.js.map