﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using VVE.Hortus.Domain.Documents;
using VVE.Hortus.Domain.Pictures;

namespace VVE.Hortus.Database
{
    public class VVEHortusRepository : IVVEHortusRepository
    {
        private readonly VVEHortusContext _ctx;
        private readonly ILogger<VVEHortusRepository> _log;

        public VVEHortusRepository(VVEHortusContext ctx, ILogger<VVEHortusRepository> log)
        {
            _ctx = ctx;
            _log = log;
        }

        public void AddEntity(object model)
        {
            _ctx.Add(model);
        }

        public IEnumerable<Picture> GetAllPictures()
        {
            try
            {
                return _ctx.Pictures
                    .OrderBy(p => p.Name)
                    .ToList();
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get all pictures: {ex}");
                return null;
            }
        }

        public Picture GetPictureById(int pictureId)
        {
            try
            {
                return _ctx.Pictures
                    .Where(p => p.Id == pictureId)
                    .FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get picture(s) by Id: {ex}");
                return null;
            }
        }

        public IEnumerable<Document> GetAllDocuments()
        {
            try
            {
                return _ctx.Documents
                    .OrderBy(p => p.FileName)
                    .ToList();

            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get all files: {ex}");
                return null;
            }
        }
        public bool SaveAll()
        {
            try
            {
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get picture(s) by Id: {ex}");
                return false;
            }

            return true;
        }

        public Document GetDocumentById(int id)
        {
            try
            {
                return _ctx.Documents
                    .Where(p => p.Id == id)
                    .FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get picture(s) by Id: {ex}");
                return null;
            }
        }
    }
}
