﻿using System.Collections.Generic;
using VVE.Hortus.Domain.Documents;
using VVE.Hortus.Domain.Pictures;

namespace VVE.Hortus.Database
{
    public interface IVVEHortusRepository
    {
        void AddEntity(object model);
        IEnumerable<Picture> GetAllPictures();
        Picture GetPictureById(int pictureId);
        bool SaveAll();
        Document GetDocumentById(int id);
        IEnumerable<Document> GetAllDocuments();
    }
}