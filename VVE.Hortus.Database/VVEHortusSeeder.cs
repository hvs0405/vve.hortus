﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VVE.Hortus.Domain.Documents;
using VVE.Hortus.Domain.Pictures;
using VVE.Hortus.Domain.Users;

namespace VVE.Hortus.Database
{
    public class VVEHortusSeeder
    {
        private readonly IHostingEnvironment _hosting;
        private readonly VVEHortusContext _ctx;
        private readonly UserManager<StoreUser> _userManager;

        public VVEHortusSeeder(VVEHortusContext ctx, IHostingEnvironment hosting, UserManager<StoreUser> userManager)
        {
            _hosting = hosting;
            _ctx = ctx;
            _userManager = userManager;
        }

        public async Task SeedAsync()
        {
            _ctx.Database.EnsureCreated();

            StoreUser user = await _userManager.FindByEmailAsync("hvs0405@gmail.com");
            if (user == null)
            {
                user = new StoreUser()
                {
                    FirstName = "Henk",
                    LastName = "van Schaik",
                    Email = "hvs0405@gmail.com",
                    UserName = "hvs0405@gmail.com",
                };

                var result = await _userManager.CreateAsync(user, "P@ssw0rd!");
                if (result != IdentityResult.Success)
                {
                    throw new InvalidOperationException("Could not create new user in seeder");
                };
            }

            if (!_ctx.Pictures.Any())
            {

                var filePath = Path.Combine(_hosting.ContentRootPath, "Pictures.json");
                var json = File.ReadAllText(filePath);

                var pictures = JsonConvert.DeserializeObject<IEnumerable<Picture>>(json);
                foreach (var p in pictures)
                {
                    p.User = user;
                }

                _ctx.Pictures.AddRange(pictures);

                _ctx.SaveChanges();
            }

            if (!_ctx.Documents.Any())
            {
                var filePath = Path.Combine(_hosting.ContentRootPath, "Documents.json");
                var json = File.ReadAllText(filePath);

                var docs = JsonConvert.DeserializeObject<IEnumerable<Document>>(json);

                _ctx.Documents.AddRange(docs);

                _ctx.SaveChanges();
            }
        }
    }
}
