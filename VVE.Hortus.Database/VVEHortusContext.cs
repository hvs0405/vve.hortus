﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VVE.Hortus.Domain.Documents;
using VVE.Hortus.Domain.Pictures;
using VVE.Hortus.Domain.Users;

namespace VVE.Hortus.Database
{
    public class VVEHortusContext : IdentityDbContext<StoreUser>
    {
        public VVEHortusContext(DbContextOptions<VVEHortusContext> options) : base(options)
        {
        }

        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Document> Documents { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Document>()
                .HasIndex(p => new { p.Year, p.FileName });
        }

    }
}
